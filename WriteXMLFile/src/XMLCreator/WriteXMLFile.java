package XMLCreator;
//Mariusz Krzy�ak

import java.io.*;
import jdom.Attribute;
import jdom.Document;
import jdom.Element;
import jdom.output.Format;
import jdom.output.XMLOutputter;

public class WriteXMLFile {
	public static void main(String[] args) throws IOException {
		int counter;
		String s, a, t, p, sf,dev;

		System.out.print("Podaj liczbe urzadzen: ");
		BufferedReader stdin = new BufferedReader(new InputStreamReader(
				System.in), 1);
		s = stdin.readLine();
		counter = Integer.parseInt(s);

		try {

			System.out.print("Podaj type devices: ");
			BufferedReader stdin0 = new BufferedReader(
					new InputStreamReader(System.in), 1);
			dev = stdin0.readLine();
			
			Element devices = new Element("devices");
			devices.setAttribute(new Attribute("type", dev));

			Document doc = new Document(devices);
			doc.setRootElement(devices);

			for (int i = 1; i <= counter; i++) {

				System.out.print("Podaj liczbe adres: ");
				BufferedReader stdin1 = new BufferedReader(
						new InputStreamReader(System.in), 1);
				a = stdin1.readLine();

				System.out.print("Podaj liczbe typ: ");
				BufferedReader stdin2 = new BufferedReader(
						new InputStreamReader(System.in), 1);
				t = stdin2.readLine();

				System.out.print("Podaj liczbe platforme: ");
				BufferedReader stdin3 = new BufferedReader(
						new InputStreamReader(System.in), 1);
				p = stdin3.readLine();

				System.out.print("Podaj liczbe space_frag: ");
				BufferedReader stdin4 = new BufferedReader(
						new InputStreamReader(System.in), 1);
				sf = stdin4.readLine();

				Element device = new Element("device");

				device.setAttribute(new Attribute("adres", a));
				device.setAttribute(new Attribute("type", t));
				device.setAttribute(new Attribute("platform", p));
				device.setAttribute(new Attribute("space_frag", sf));

				doc.getRootElement().addContent(device);

			}

			
			for (int i = 0; i < counter; i++) {
				XMLOutputter xmlOutput = new XMLOutputter();

				xmlOutput.setFormat(Format.getPrettyFormat());

				//xmlOutput.output(doc, new FileWriter("/Users/mariuszkrzyzak/Desktop/config.xml"));
				xmlOutput.output(doc, new FileWriter("C:\\config.xml"));

			}
			System.out.println("File Saved!");
		} catch (IOException io) {
			System.out.println(io.getMessage());
		}
	}
}
