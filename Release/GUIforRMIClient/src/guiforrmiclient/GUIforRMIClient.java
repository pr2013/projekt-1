/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package guiforrmiclient;

import cmd.execute.ExecuteCMD;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author paweln66, Mateusz
 */
public class GUIforRMIClient extends javax.swing.JFrame implements ActionListener {

    private String ip;
    private String port;
    private String pathResult = "C:\\Client\\";
    private String processingUnit = "CPU";
    private String numberOfProcessingUnit = "1";
    private String clientDirPath = "C:\\Client";
    ExecuteCMD execCMD;
    private String runClientCMD = "java -cp .;classes\\compute.jar;classes\\jdom-1.1.jar -Djava.rmi.server.codebase=file:classes\\ -Djava.security.policy=client.policy client.ComputeClient ";

    public GUIforRMIClient() {
        initComponents();

        execCMD = new ExecuteCMD();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        modelComboBox = new javax.swing.JComboBox();
        ipTextField = new javax.swing.JTextField();
        portTextField = new javax.swing.JTextField();
        selectFileButton = new javax.swing.JButton();
        sendButton = new javax.swing.JButton();
        downloadResultsButton = new javax.swing.JButton();
        processingUnitComboBox = new javax.swing.JComboBox();
        numberOfProcessingUnitComboBox = new javax.swing.JComboBox();
        quitButton = new javax.swing.JButton();
        acceptButton = new javax.swing.JButton();
        fileChooser = new javax.swing.JFileChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        modelComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"dynamic_recrystallization", "fatigue_cracking", "naive_growth"}));

        ipTextField.setText("127.0.0.1");

        portTextField.setText("1099");

        selectFileButton.setText("Select folder");
        selectFileButton.addActionListener(this);

        sendButton.setText("Download");
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
                    sendButtonActionPerformed(evt);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(GUIforRMIClient.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(GUIforRMIClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        downloadResultsButton.setText("Set Config");
        downloadResultsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downloadResultsButtonActionPerformed(evt);
            }
        });

        processingUnitComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"CPU", "GPU"}));
        processingUnitComboBox.addActionListener(this);

        numberOfProcessingUnitComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"1", "2", "3", "4"}));
        numberOfProcessingUnitComboBox.addActionListener(this);

        quitButton.setText("Quit");
        quitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitButtonActionPerformed(evt);
            }
        });

        acceptButton.setText("Launch");
        acceptButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                acceptButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(quitButton)
                .addGroup(layout.createSequentialGroup()
                .addComponent(modelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(processingUnitComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(numberOfProcessingUnitComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(ipTextField)
                .addGroup(layout.createSequentialGroup()
                .addComponent(selectFileButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(sendButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(downloadResultsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createSequentialGroup()
                .addComponent(portTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(acceptButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap(30, Short.MAX_VALUE)));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(ipTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(portTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(acceptButton))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(downloadResultsButton)
                .addComponent(sendButton)
                .addComponent(selectFileButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(modelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(processingUnitComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(numberOfProcessingUnitComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(quitButton)
                .addContainerGap(19, Short.MAX_VALUE)));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sendButtonActionPerformed(java.awt.event.ActionEvent evt) throws FileNotFoundException, IOException {//GEN-FIRST:event_sendButtonActionPerformed
        BufferedReader br = new BufferedReader(new FileReader("C:\\ServerRMI\\tmp\\" + modelComboBox.getSelectedItem().toString() + ".txt"));
        StringBuilder sb;
        String everything = "";
        try {

            sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            everything = sb.toString();
        } catch (Exception e) {
        } finally {
            br.close();
        }

        try {
            // Create file 
            FileWriter fstream = new FileWriter(pathResult + "\\" + modelComboBox.getSelectedItem().toString() + ".txt");
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(everything);
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }//GEN-LAST:event_sendButtonActionPerformed

    private void acceptButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_acceptButtonActionPerformed
        this.ip = this.ipTextField.getText();
        this.port = this.portTextField.getText();

        int returnCode = execCMD.exec(new String[]{"cd " + this.clientDirPath,
            runClientCMD + this.ip + " runFW#model=" + modelComboBox.getSelectedItem().toString() + "#pathSrc=C:\\ServerRMI\\space100.txt"});
        if (returnCode != 0) {
            JOptionPane.showMessageDialog(this, "Return code: " + returnCode);
        }
    }//GEN-LAST:event_acceptButtonActionPerformed

    private void downloadResultsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downloadResultsButtonActionPerformed
        this.ip = this.ipTextField.getText();
        this.port = this.portTextField.getText();

        int returnCode = execCMD.exec(new String[]{"cd " + this.clientDirPath,
            runClientCMD + this.ip + " createXML#" + processingUnitComboBox.getSelectedItem().toString() + "#" + numberOfProcessingUnitComboBox.getSelectedItem().toString()});
        if (returnCode != 0) {
            JOptionPane.showMessageDialog(this, "Return code: " + returnCode);
        }
    }//GEN-LAST:event_downloadResultsButtonActionPerformed

    private void quitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitButtonActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_quitButtonActionPerformed

    @Override
    public void actionPerformed(java.awt.event.ActionEvent e) {
        if (e.getSource() == selectFileButton) {
            int returnVal = fileChooser.showOpenDialog(GUIforRMIClient.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getCurrentDirectory();
                this.pathResult = file.getPath();
            }
        }

        if (e.getSource() == processingUnitComboBox) {
            if (processingUnitComboBox.getSelectedItem() == "CPU") {
                processingUnit = "CPU";
                numberOfProcessingUnitComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"1", "2"}));
                numberOfProcessingUnitComboBox.setSelectedItem(numberOfProcessingUnit);
            } else if (processingUnitComboBox.getSelectedItem() == "GPU") {
                processingUnit = "GPU";
                numberOfProcessingUnitComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"1", "2", "3", "4"}));
                if (numberOfProcessingUnit.equals("1") || numberOfProcessingUnit.equals("2")) {
                    numberOfProcessingUnitComboBox.setSelectedItem(numberOfProcessingUnit);
                } else {
                    this.numberOfProcessingUnit = (String) numberOfProcessingUnitComboBox.getSelectedItem();
                }
            }
        }

        if (e.getSource() == numberOfProcessingUnitComboBox) {
            this.numberOfProcessingUnit = (String) numberOfProcessingUnitComboBox.getSelectedItem();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(GUIforRMIClient.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUIforRMIClient().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton acceptButton;
    private javax.swing.JButton downloadResultsButton;
    private javax.swing.JTextField ipTextField;
    private javax.swing.JComboBox modelComboBox;
    private javax.swing.JComboBox numberOfProcessingUnitComboBox;
    private javax.swing.JTextField portTextField;
    private javax.swing.JComboBox processingUnitComboBox;
    private javax.swing.JButton quitButton;
    private javax.swing.JButton selectFileButton;
    private javax.swing.JButton sendButton;
    private javax.swing.JFileChooser fileChooser;
    // End of variables declaration//GEN-END:variables

    public void setPath(String path) {
        this.pathResult = path;
    }
}
