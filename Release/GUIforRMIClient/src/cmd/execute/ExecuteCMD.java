package cmd.execute;

import java.io.*;

/**
 *
 * @author paweln66
 */
public class ExecuteCMD {

    public int exec(String[] args) {
        try {
            String[] command = {"cmd",};

            Process p = Runtime.getRuntime().exec(command);

            (new Thread(new SyncPipe(p.getErrorStream(), System.err))).start();
            (new Thread(new SyncPipe(p.getInputStream(), System.out))).start();

            PrintWriter stdin = new PrintWriter(p.getOutputStream());

            for (int i = 0; i < args.length; i++) {
                stdin.println(args[i]);
            }


            stdin.close();
            int returnCode = p.waitFor();


            return returnCode;

        } catch (Exception e) {
        }
        return -2;
    }
}
