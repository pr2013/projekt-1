package client;

import compute.Task;
import java.io.Serializable;
import java.io.*;

public class ExecuteExe implements Task<String>, Serializable {

    private static final long serialVersionUID = 227L;

	private final String[] param;

	/**
	 * Construct a task to calculate
	 */
	public ExecuteExe(String[] param) {
		this.param = param;
	}

	/**
	 * Calculate
	 */
	public String execute() {
		String s;
		String tmp = "";
		
		try {
			
			String[] cmd = new String[param.length + 1]; //{ "C:\\HelloRMI", "dupa" }; // nazwa prog exe
			cmd[0] = "C:\\ServerRMI\\FrmwkCAMicroStruct";
			for(int i = 0; i < param.length; i++)
			{
				cmd[i + 1] = param[i];
			}
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
					

			while ((s = br.readLine()) != null) {
				System.out.println(s);
				// osw.write(s, 0, s.length());//Zapis do pliku
				tmp += s;
			}
			System.out.println("\tExecution file done.");
			// osw.close();//Zapis do pliku
			br.close();
			return tmp;
			
		} catch (Exception exc) {
			return "Exception has been thrown";
		}
		

	}
}