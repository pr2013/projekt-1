package client;

import compute.Task;
import java.io.Serializable;
import java.io.*;

import org.jdom.Content;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class GenerateConfigXML implements Task<String>, Serializable {
	
	public String type;
	public int amount;

	public GenerateConfigXML(String[] param)
	{
		type = param[0];
		amount = Integer.parseInt(param[1]);
	}

	public String execute() {
	
		try {
		
			Element devices = new Element("devices");
			devices.setAttribute(new Attribute("type", "local"));

			Document doc = new Document(devices);
			doc.setRootElement(devices);
			
			for (int i = 1; i <= amount; i++) 
			{
				Element device = new Element("device");
			
				device.setAttribute(new Attribute("adres", "local"));
				device.setAttribute(new Attribute("type", type));
				device.setAttribute(new Attribute("platform", "nvidia"));
				device.setAttribute(new Attribute("space_frag", "0.5"));
				
				doc.getRootElement().addContent(device);
			}	
			
			for (int i = 0; i < amount; i++) {
				XMLOutputter xmlOutput = new XMLOutputter();

				xmlOutput.setFormat(Format.getPrettyFormat());

				//xmlOutput.output(doc, new FileWriter("/Users/mariuszkrzyzak/Desktop/config.xml"));
				xmlOutput.output(doc, new FileWriter("C:\\ServerRMI\\config.xml"));

			}	
		} catch (IOException io) {
			System.out.println(io.getMessage());
			return "Exception occured during XML create";
		}
	
		return "Succed: configXML";
	}
}