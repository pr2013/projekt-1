package client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.math.BigDecimal;
import org.jdom.Content;

import compute.Compute;

public class ComputeClient {
    public static void main(String args[]) {
	
		String[] arguments = args[1].split("#");
		String eventName = arguments[0];
		String[] aVC = {args[0], eventName};
		
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = "Compute";
            Registry registry = LocateRegistry.getRegistry(args[0]);
            Compute comp = (Compute) registry.lookup(name);
						
			
			//String result = callVectorClock(aVC)
			VectorClock vc = new VectorClock(aVC);
			String result = comp.executeTask(vc);
			String[] host = result.split("\\|");
			
			//prevent of deathlock
			if(host[0].equals(args[0])){
				
				if(eventName.equals("runFW"))
				{
					String[] params = {arguments[1], arguments[2]};

					ExecuteExe ee = new ExecuteExe(params);
					String messageExe = comp.executeTask(ee);
					
					System.out.println("Succeed run exe file: " + messageExe);
				}
				else if(eventName.equals("createXML"))
				{
					String[] configParam = {arguments[1], arguments[2]};
			
					GenerateConfigXML config = new GenerateConfigXML(configParam);
					String messageXML = comp.executeTask(config);
					
					System.out.println(messageXML);
				}
				else{
					System.out.println("dupa");
				}
			}
        } catch (Exception e) {
            System.err.println("ComputeClient exception:");
            e.printStackTrace();
        }
    }

/*	private String callVectorClock(String[] aVC){
		System.out.println("dupa VC");
		
		return result;
	}	*/
}