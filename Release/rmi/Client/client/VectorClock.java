package client;

import compute.Task;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class Pair{ 
	private String value1;
	private int value2;
	Pair(String x, Integer y){
		value1 = x;
		value2 = y;
	}
    public String getValue1() { return value1; }
    public int getValue2() { return value2; }
    public void setValue1(String x) { value1 = x; }
    public void setValue2(Integer y) { value2 = y; }
} 

public class VectorClock implements Task<String>, Serializable  {
	private List<String> fifo;
	private List<Pair> vector;
	private String host, event_name;
	
	public VectorClock(String param[]){
		this.host = param[0];
		this.event_name = param[1];
		
		fifo = new ArrayList<String>();
		fifo.add(this.host);
		vector = new ArrayList<Pair>();
		/*Pair p = new Pair(host, 0);
		vector.add(p);*/
	}
	
	public String execute() {
		riseOperationCoutner();
	
		String result = getFirstFromList() + "|ip:anount_event:" + this.event_name;	//TODO MK: tutaj musi byc zegar w postaci string opis w stringu, np: |127.0.0.1:2 259.112.198.12:1:run FORMAT TEN TO: ip:anount_event:evant_name
		System.out.println("\tVectorClock: " + result);
		return result; 	//Zwracamy pierwszego z listy i tre�� zegara oddzielon� znakiem |  
	}
	/*public void addUser(String userName){
		Pair p = new Pair(userName, 0);
		vector.add(p);
		fifo.add(userName);
	}*/
	
	//TODO MK: ta metoda ma by� wywo�ywana w metodzie execute(). Musi ona z pliku VectorClock.log (odpowiednik pliku .txt) odczytywa� aktualny stan wektora i dodawa� nowych user�w po IP (je�li ich nie ma), je�li s� musi zwi�ksza� licznik eventu i //nadpisywa� ten plik .log
	public void riseOperationCoutner(){
		Iterator<Pair> iter = vector.iterator();
		while(iter.hasNext()){
			Pair p = iter.next();
			if(p.getValue1().equals(this.host)){
				p.setValue2(p.getValue2()+1);
			}
		}
	}
	
	private String getFirstFromList(){
		String firstOne = fifo.get(0);
		fifo.remove(0);
		return firstOne;
	}
}