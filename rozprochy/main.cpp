#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include "ca.hpp"

 #define TESTING 

const char * const  KERNEL_FILE 			= "kernels/static_recrystallization.cl" ;
const char * const  CREATING_GRAINS_KERNEL_FILE         = "kernels/creating_grains.cl" ;
const char * const  TEST_KERNEL				= "kernels/forTesting.cl";
const char * const  NAIVE_GROWTH_KERNEL			= "kernels/naive_growth.cl";

const int NUM_OF_GREMS = 3000;
const int HEIGHT_SPACE = 100;
const int WIDTH_SPACE = 100;

using namespace std;

int main() {
	ca::space_size size[2] = {WIDTH_SPACE, HEIGHT_SPACE};
	long num_cells = size[0]*size[1];

	ca::Space space;

	ca::Cell::CellConf conf = ca::Cell::CellConf(2,2);

	ca::Cell * cells;
	cells = new ca::Cell[num_cells];
	int * num_gr = new int[num_cells];

	for(int i=0; i<num_cells; ++i){
		cells[i].init(conf);
	}

#if defined(TESTING)
	char * inputFile = "input100"; 
#elif defined(TESTING2)
        char * inputFile = "spaceRAND.txt";
#elif defined(GRAIN_GROWTH)
	char * inputFile = "spaceRAND.txt";
#elif defined(FATIGUE_CRACK)
	char * inputFile = "input100";
#elif defined(RECRYSTAL)
	char * inputFile = "input100";
#else
	char * inputFile = "";
#endif	

	ifstream inp(inputFile);	

	for(int i = 0; i < num_cells; i++){
		char temp;
        	inp >> temp;
        	if(temp != '\n'){
            		num_gr[i] = atoi(&temp);
	//		cout << num_gr[i];
        	}
    	}

	inp.close();


///////////////////////// CELLs /////////////////////////////////////////////////////////

	ca::GrainList* grainList = &space.getGrainList();
//	(*grainList).setNumVarible(1,2);
	(*grainList).setNumVarible(0,0);	

	ca::Grain* germs = (*grainList).newGrains(NUM_OF_GREMS);

	srand(time(0));
	int tmp = 0;

	for(int i=0; i<100; ++i){
		for(int j=0; j<100;++j){
#if defined(TESTING)			
			cells[j + i*100].setGrain(germs[num_gr[0]])	//set grain ID
					.setInt(0, 1)			//set value of int
					.setInt(1, 9)
					.setReal(0, 3.0)		//set value of real
					.setReal(1, 7.0);
#elif defined(TESTING2)	
			cells[j + i*100].setGrain(germs[num_gr[j + i*100]]);
#elif defined(GRAIN_GROWTH)
			if ( num_gr[j + i*100] != 0 ){
				cells[j + i*100].setGrain(germs[num_gr[j + i*100]]);
			}
#elif defined(FATIGUE_CRACK)
			cells[j + i*100].setGrain(germs[num_gr[0]])     //set grain ID
                                        .setInt(0, 1)                   //set value of int
                                        .setReal(0, 3.0)                //set value of real
                                        .setReal(1, 7.0)
					.setReal(2, 8.0);		
#elif defined(RECRYSTAL)
			cells[j + i*100].setGrain(germs[num_gr[0]])     //set grain ID
                                        .setInt(0, 1)                   //set value of int
					.setInt(1, 1)
					.setInt(2, 1)
                                        .setReal(0, 3.0);                //set value of real
#endif	
	}

	}

	space.setCells(size, ca::DIM_2, cells);
	space.setCellConf(conf);
	space.setBoundaryCondition(ca::Space::PERIODIC_CONDITION);

//////////////////////////////////////////////////////////////////////////////////////

	ca::Model cr_grains(space);
	cr_grains.getMemoryManager().setMode(ca::MemoryManager::ONE_ITERATION_SENDING_WHOLE_SPACE_MODE);

	ca::NeighbourScheme neighb("src/neuman.ngh");
//	ca::Rule mainRule("test_function");

#if defined(TESTING)
        ca::Rule mainRule("create_grains");
#elif defined(TESTING2)
        ca::Rule mainRule("test_function");
#elif defined(GRAIN_GROWTH)
        ca::Rule mainRule("naive_grain_growth");
#elif defined(FATIGUE_CRACK)
        ca::Rule mainRule("fatigue_cracking");
#elif defined(RECRYSTAL)
	ca::Rule mainRule("static_recrystallization");
#endif

	ca::Event mainEvent(neighb,mainRule);
//	mainEvent.addKernelSourceFile(TEST_KERNEL);

#if defined(TESTING)
        mainEvent.addKernelSourceFile(CREATING_GRAINS_KERNEL_FILE);
#elif defined(TESTING2)
        mainEvent.addKernelSourceFile(TEST_KERNEL);
#elif defined(GRAIN_GROWTH)
        mainEvent.addKernelSourceFile(NAIVE_GROWTH_KERNEL);
#elif defined(FATIGUE_CRACK)
        mainEvent.addKernelSourceFile(FATIGUE_CRACKING);
#elif defined(RECRYSTAL)
        mainEvent.addKernelSourceFile(KERNEL_FILE);
#endif

	cr_grains.getKernelManager().setMainEvent(mainEvent);
	cr_grains.Init();
	cr_grains.run(100);

	
	delete[] cells;
	delete[] num_gr;
	return 0;
}
