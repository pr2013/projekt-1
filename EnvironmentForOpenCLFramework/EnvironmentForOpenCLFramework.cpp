/* 
 * File:   main.cpp
 * Author: paweln66
 *
 * Created on 23 listopad 2012, 21:52
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <math.h>
#include <fstream>

using namespace std;

#define NUM_OF_GREMS 3000
#define HEIGHT_SPACE 100
#define WIDTH_SPACE 100
#define INT_VAR_NUM 3
#define REAL_VAR_NUM 1
#define NUM_NEIGH 4
#define NUM_STEPS 1000
#define mwc64x_state_t int

/*declaration*/
int stepGlobal;
int newGrain;
int gx, gy;
int *__beg_index = 0;
int *__end_index = 0;
const int X_SIZE = HEIGHT_SPACE;
const int Y_SIZE = WIDTH_SPACE;
void /*__rule*/ static_recrystallization();

/*definition*/
struct uint2{
    unsigned int x;
    unsigned int y;
};

int cell_grain[WIDTH_SPACE][HEIGHT_SPACE];
int cell_grain_w[WIDTH_SPACE][HEIGHT_SPACE];
int CELL_GRAIN(int gx, int gy){
    return cell_grain[gx][gy];
}
int & CELL_GRAIN_W(int gx, int gy){
    return cell_grain_w[gx][gy];
}

int cell_int[INT_VAR_NUM][WIDTH_SPACE][HEIGHT_SPACE];
int cell_int_w[INT_VAR_NUM][WIDTH_SPACE][HEIGHT_SPACE];
int CELL_INT(int gx, int gy, int varNum){
    return cell_int[varNum][gx][gy];
}
int & CELL_INT_W(int gx, int gy, int varNum){
    return cell_int_w[varNum][gx][gy];
}

double grain_real[REAL_VAR_NUM][WIDTH_SPACE][HEIGHT_SPACE];
double grain_real_w[REAL_VAR_NUM][WIDTH_SPACE][HEIGHT_SPACE];
double CELL_REAL(int gx, int gy, int varNum){
    return grain_real[varNum][gx][gy];
}
double & CELL_REAL_W(int gx, int gy, int varNum){
    return grain_real_w[varNum][gx][gy];
}

int __ints[1];
void MWC64X_SeedStreams(int * rng, int __ints, int z){
}

int MWC64X_NextUint(int *rng){
    srand ( time(NULL) );
    return rand() % 100;
}

uint2 tmp;
uint2 &getNeighbourhood(int gx, int gy, int i, int &__beg_index, int &__end_index){
    //uint2 tmp;
    if(i == 0){    //up
        int x = gx -1;
        if(x < 0) tmp.x = WIDTH_SPACE - 1;
        else tmp.x = x;
        tmp.y = gy;
        return tmp;
    }
    else if(i == 1){    //right
        tmp.x = gx;
        int y = gy + 1;
        if(y > HEIGHT_SPACE - 1) tmp.y = 0;
        else tmp.y = y;
        return tmp;
    }
    else if(i == 2){    //down
        int x = gx + 1;
        if(x > WIDTH_SPACE - 1) tmp.x = 0;
        else tmp.x = x;
        tmp.y = gy;
        return tmp;
    }
    else if (i == 3){    //left
        tmp.x = gx;
        int y = gy -1;
        if(y < 0) tmp.y = HEIGHT_SPACE - 1;
        else tmp.y = y;
        return tmp;
    }
}

void updateArrays(){

    for(int x = 0;  x < WIDTH_SPACE; x++){
        for(int y = 0; y < HEIGHT_SPACE; y++){
            cell_grain[x][y] = cell_grain_w[x][y];
            for (int sInt = 0; sInt < INT_VAR_NUM; sInt++) cell_int[sInt][x][y] = cell_int_w[sInt][x][y];
            for (int sReal = 0; sReal < REAL_VAR_NUM; sReal++) grain_real[sReal][x][y] = grain_real_w[sReal][x][y];
            }
    }
}

/*
 * 
 */
int main(int argc, char** argv) {
    
    std::ifstream file;
    file.open("space100.txt");
    
    for(int i = 0;  i < WIDTH_SPACE; i++){
        for(int j = 0; j < HEIGHT_SPACE; j++){
            cell_grain_w[i][j] = 0;
            file >> cell_grain_w[i][j];
            for(int k = 0; k<3; k++)    cell_int_w[k][i][j] = 0;
            grain_real_w[0][i][j] = 0;

            std::cout << cell_grain_w[i][j] << " ";
        }
        std::cout << endl;
    }
    
    file.close();

    /*grain[1][1] = 1;
    grain[3][1] = 2;
    grain[4][3] = 3;
    grain[5][2] = 4;
    GRAIN_W(1, 1) = 1;
    GRAIN_W(3, 1) = 2;
    GRAIN_W(4, 3) = 3;
    GRAIN_W(5, 2) = 4;*/

    //grain_int_w[2][0][0] = 5;
    newGrain = 10;

    for(stepGlobal = 0; stepGlobal < NUM_STEPS; stepGlobal++){
        updateArrays();
        for(gx = 0;  gx < WIDTH_SPACE; gx++){
            for(gy = 0; gy < HEIGHT_SPACE; gy++){
                static_recrystallization();
            }
        }
        //int brk;////////////////////////////////////////////////////////
        //        if(stepGlobal == 9998)////////////////////////////////////////////////
        //            brk = 0;////////////////////////////////////////////////////
    }

    std::ofstream fileOut;
    fileOut.open("result.txt", ios::trunc);

    std::cout << "\n\nOUTPUT:\n";
    for(int i = 0;  i < WIDTH_SPACE; i++){
        for(int j = 0; j < HEIGHT_SPACE; j++){
            std::cout << cell_grain[i][j] << " ";
            fileOut << cell_grain[i][j] << " ";
        }
        std::cout << endl;
        fileOut << endl;
    }
    fileOut.close();

    return 0;
}

void /*__rule*/ static_recrystallization(){

/*INFO about variable for grain*/
//CELL_GRAIN(gx, gy, 0); --> var1 if Recruystallized in previous step    (it can be cell)
//CELL_GRAIN(gx, gy, 1); --> var2 step number because not accessable from framework  (it should be grain)
//CELL_GRAIN(gx, gy, 2); --> var3 new grain id   (if should be grain)
//CELL_GRAIN(gx, gy, 0); --> var1 amount of energy  (it should be cell)

	//INITIALIZE
	uint2 temp;	//neighmoubr position
        CELL_INT_W(gx, gy, 1) = CELL_INT(gx, gy, 1) + 1;	//increase step
	int step = CELL_INT(gx, gy, 1);	//remember step number
	int recrystallized = 0; //if trigger nucleation in this step; 0 if no, 1 if yes
        int recrystNghPrv = 0; //if trigger nucleation in previos step;
        int isOnBoard = 0; //0 if no, 1 if no
	int greaterEnergyThanNgh = 0;	//amount of neighbour with greater energy
	float A = 86710969050178.5;
	float B = 9.41268203527779;
	float t = 0.001 * step;
        int dimArea = X_SIZE * Y_SIZE;
	float criticalEnergy = 1804114860541290 / (float)dimArea ;	/* for 300 STEPS */

        //compute site coverage by recrystallization front
	mwc64x_state_t rng;	// ustawianie liczby losowej
	MWC64X_SeedStreams(&rng, __ints[0], 3455);
	int rand = MWC64X_NextUint(&rng);

	//Verify if grain is on board and if neighbour recrystallized in previous step
	for(int i = 0; i< NUM_NEIGH; i++){
		temp = getNeighbourhood(gx, gy, i, *__beg_index, *__end_index);
		if (CELL_GRAIN(gx, gy) != CELL_GRAIN(temp.x, temp.y) && CELL_INT(temp.x, temp.y, 0) == 0){
			isOnBoard = 1;	//is on board
		}
		if (CELL_INT(temp.x, temp.y, 0) == 1){
			recrystNghPrv++;	//amount of neighbour recrystallized in previous step
		}
		if (CELL_REAL(temp.x, temp.y, 0) < CELL_REAL(gx, gy, 0)){
			greaterEnergyThanNgh ++;
		}
                else if(CELL_INT(temp.x, temp.y, 0) != 0)
                    greaterEnergyThanNgh ++;   //energy is not unclude if grain is recrystalized
	}

        //compute site coverage by other grains
	float energy = (1.0 - A/B) * exp((-B) * t) + A/B;		//amount of energy for grain
	float energyPerCell = energy / (float)dimArea;

        if(isOnBoard == 1 && recrystallized == 0){
            float rand4 = (float)(rand % 4);
        	float dRand4 = rand4 * 0.1;
        	float dmRand4 = 1 - dRand4;
		float randEnergyPerCell = dmRand4 * energyPerCell;
		float valReal0 = CELL_REAL(gx, gy, 0);
		float valToSet = randEnergyPerCell + valReal0;
        	CELL_REAL_W(gx, gy, 0) = valToSet;
            //CELL_REAL_W(gx, gy, 0) = ((1.0 - (float)(rand % 4) * 0.1) * energyPerCell) + CELL_REAL(gx, gy, 0);
	}
	else{
            if(isOnBoard == 0 && recrystallized == 0){
                float rand4 = (float)(rand % 4);
                float dRand4 = rand4 * 0.1;
		CELL_REAL_W(gx, gy, 0) = dRand4 * energyPerCell + CELL_REAL(gx, gy, 0);
                //CELL_REAL_W(gx, gy, 0) = ((float)(rand % 4) * 0.1 * energyPerCell) + CELL_REAL(gx, gy, 0);
            }
	}
        if(recrystNghPrv != 0){
            CELL_REAL_W(gx, gy, 0) = 0;
        }

	//SDX CONDITION 1   *PROPAGATION*
	/*IF in previous step one neighbour trigger nucleation and
	if neighbour energy is less than in the particular cell*/
	if (recrystNghPrv != 0 && greaterEnergyThanNgh >= (NUM_NEIGH - 1) && CELL_INT(gx, gy, 0) == 0){
            int numGrain = (rand % recrystNghPrv);
            int iter = 0;
            for(int i = 0; i< NUM_NEIGH; i++){
                temp = getNeighbourhood(gx, gy, i, *__beg_index, *__end_index);
                if(CELL_INT(temp.x, temp.y, 0) == 1){  //if recrystalized in previous step
                    //iter;
//                    if(iter == numGrain){   //verify if is more than one grain and get random of them
                        recrystallized = 1;
                        CELL_GRAIN_W(gx, gy) = CELL_GRAIN(temp.x, temp.y);
                        CELL_REAL_W(gx, gy, 0) = 0;
//                   }
              }
            }
	}
	//SDX CONDITION 2   *NUCLEATION*
	/*IF cell is on board and the amount of energy in the partuicular cell is
	the critical amount of energy which is necessary to trigger nucleation.
        Also any neighbour can't be recrystalized*/
	else{
            if(CELL_REAL(gx, gy, 0) > criticalEnergy /*&& isOnBoard == 1*/ && CELL_INT(gx, gy, 0) == 0 && recrystNghPrv == 0  && greaterEnergyThanNgh >= (NUM_NEIGH - 1)){
                //if( ((double)(rand % 10) * 0.1) <= 0.4){    //40%
                if (rand % 2 == 0){
                    recrystallized = 1;
                    CELL_GRAIN_W(gx, gy) = newGrain++;   //PROBLEM WITH NEW GRAIN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
                    //GRAIN_W(gx, gy) = GRAIN_Int(0, 0, 2);   //PROBLEM WITH NEW GRAIN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
                    //GRAIN_Int_W(0, 0, 2) = GRAIN_Int(0, 0, 2) + 1;
                    std::cout << "Grain id" << (newGrain - 1) << " recr in step #" << CELL_INT(gx, gy, 1) << std::endl;
                }
                else {
                    CELL_REAL_W(gx, gy, 0) = 0.5 * CELL_REAL(gx, gy, 0);
                }
            }
	}

        double brk;////////////////////////////////////////////////////////
                //if(GRAIN_Int(gx, gy, 1) == 70 && isOnBoard == 1)////////////////////////////////////////////////
                if(CELL_REAL(gx, gy, 0) > criticalEnergy)
                    brk = CELL_REAL(gx, gy, 0);////////////////////////////////////////////////////
	
	//set flag that grain trigger nucleation
	if(recrystallized == 1){
		CELL_INT_W(gx, gy, 0) = 1;
	}
	//else{
	//	GRAIN_Int_W(gx, gy, 0) = 0;
	//}
}
