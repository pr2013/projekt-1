package exec;

import java.io.*;

public class ExecRun {

	public static void main(String argv[]) {
		try {
			String s;
			String[] cmd = { "cmdline" }; //nazwa prog exe
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			/*OutputStreamWriter osw = new OutputStreamWriter( //Zapis do pliku
					//new FileOutputStream(new File("nazwa.txt"), true)); //Zapis do pliku
			*/
			while ((s = br.readLine()) != null) {
				System.out.println(s);
				//osw.write(s, 0, s.length());//Zapis do pliku
			}

			//osw.close();//Zapis do pliku
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
